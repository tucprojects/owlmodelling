package jenacourier;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class Main {
    
    /**
     * Given a filename, the complete URI of the ontology is returned according
     * on what operating system the application runs.
     * 
     * @param filename  The filename where the ontology is stored.
     * @return  The ontology URI.
     */
    public static String setOntologyUri(String filename) {
        String file = null;
        if (System.getProperty("os.name").contains("Linux"))
            file = "file:" + filename;
        else
            file = "file:///" + filename;
        return file;
    }

    /**
     * Populate the given OWL model.
     * NOTE: Only instances of classes that will be used in SPARQL queries will
     * be constructed for lesser complexity.
     *
     * UGLY CODE IS ON THE WAY! YOUR EYES WILL BLEED!
     *
     * @param model The OWL model.
     */
    public static void populateModel(OntModel model) {
        String NS = "http://www.courier.com/ontologies/courier.owl#";
        int VEHICLE_NUM = 8;
        int CUSTOMER_NUM = 40;
        int EMPLOYEE_NUM = 100;
        int COMPANY_NUM = 4;
        int DELIVERABLE_NUM = 20;
        
        // Vehicle
        ArrayList<Individual> vehicle = new ArrayList<Individual>();
        Resource v_res = model.createClass(NS + "Vehicle");
        Resource vid_res = model.createClass(NS + "VehicleID");
        Property v_pro = model.createProperty(NS, "hasVehicleID");

        for (int i = 1; i <= VEHICLE_NUM; i++) {
            Individual v_ind = model.createIndividual(NS + "Vehicle_B" + i, v_res);
            Individual vid_ind = model.createIndividual(NS + "vID_B" + i, vid_res);
            v_ind.addProperty(v_pro, vid_ind);
            vehicle.add(v_ind);
        }
        
        // Customer
        ArrayList<Individual> customer = new ArrayList<Individual>();
        Resource c_res = model.createClass(NS + "Customer");
        Resource pname_res = model.createClass(NS + "PersonName");
        Resource pid_res = model.createClass(NS + "PersonID");
        Property n_pro = model.createProperty(NS, "hasPersonName");
        Property id_pro = model.createProperty(NS + "hasPersonID");

        for (int i = 1; i <= CUSTOMER_NUM; i++) {
            Individual c_ind = model.createIndividual(NS + "Customer_B" + i, c_res);
            Individual pname_ind = model.createIndividual(NS + "NameCustomerB" + i, pname_res);
            Individual pid_ind = model.createIndividual(NS + "personID_B" + i, pid_res);
            
            c_ind.addProperty(n_pro, pname_ind);
            c_ind.addProperty(id_pro, pid_ind);
            customer.add(c_ind);
        }

        // Deliverable
        ArrayList<Individual> deliverable = new ArrayList<Individual>();
        Random r = new Random();
        Resource del_res = model.createClass(NS + "Deliverable");
        Property hds_pro = model.createProperty(NS + "hasDeliverableSource");
        Property hdd_pro = model.createProperty(NS + "hasDeliverableDestination");

        for (int i = 1; i <= DELIVERABLE_NUM; i++) {
            Individual del_ind = model.createIndividual(NS + "Deliverable_B" + i, del_res);
            //   hasDeliverableSource Customer
            del_ind.addProperty(hds_pro, customer.get(r.nextInt(CUSTOMER_NUM)));
            //   hasDeliverableDestination Customer
            del_ind.addProperty(hdd_pro, customer.get(r.nextInt(CUSTOMER_NUM)));
            deliverable.add(del_ind);
        }
        
        // Employee
        ArrayList<Individual> employee = new ArrayList<Individual>();
        Resource e_res = model.createClass(NS + "Employee");

        for (int i = 1; i <= EMPLOYEE_NUM; i++) {
            Individual e_ind = model.createIndividual(NS + "Employee_B" + i, e_res);
            Individual pname_ind = model.createIndividual(NS + "NameEpmloyeeB" + i, pname_res);
            Individual pid_ind = model.createIndividual(NS + "employeeID_B" + i, pid_res);
            e_ind.addProperty(n_pro, pname_ind);
            e_ind.addProperty(id_pro, pid_ind);
            employee.add(e_ind);
        }

        // Delivery
        ArrayList<Individual> delivery = new ArrayList<Individual>();
        Resource delivery_res = model.createClass(NS + "Delivery");
        Property hdelde_pro = model.createProperty(NS + "hasDeliveryContent");
        Property hdeldr_pro = model.createProperty(NS + "hasDeliveryDriver");
        Property hdelve_pro = model.createProperty(NS + "hasDeliveryVehicle");

        for (int i = 1; i <= DELIVERABLE_NUM; i++) {
            Individual delivery_ind = model.createIndividual(NS + "Delivery_B" + i, delivery_res);
            //   hasDeliveryContent  Deliverable
            delivery_ind.addProperty(hdelde_pro, deliverable.get(i-1));
            // hasDeliveryDriver Driver
            delivery_ind.addProperty(hdeldr_pro, employee.get(r.nextInt(COMPANY_NUM)));
            // hasDeliveryVehicle Vehicle
            delivery_ind.addProperty(hdelve_pro, vehicle.get(r.nextInt(VEHICLE_NUM)));
            delivery.add(delivery_ind);
        }
        
        // Company
        ArrayList<Individual> company = new ArrayList<Individual>();
        Resource com_res = model.createClass(NS + "Company");
        Resource cname_res = model.createClass(NS + "CompanyName");
        Property cn_pro = model.createProperty(NS, "hasCompanyName");
        Property cd_pro = model.createProperty(NS + "hasDriver");
        Property coa_pro = model.createProperty(NS + "hasOfficeAssistant");
        Property csa_pro = model.createProperty(NS + "hasStorageAssistant");
        Property cow_pro = model.createProperty(NS + "isOwnerOf");
        int company_index = 0;
        int vehicle_index = 0;
        int delivery_index = 0;

        //   hasCompanyName CompanyName
        for (int i = 1; i <= COMPANY_NUM; i++) {
            Individual com_ind = model.createIndividual(NS + "Company_B" + i, com_res);
            Individual cname_ind = model.createIndividual(NS + "nameCompany_B" + i, cname_res);
            com_ind.addProperty(cn_pro, cname_ind);
            
            for (int k = 1; k <= EMPLOYEE_NUM / 4; k++) {
                if (k >= 1 && k <=9 ) {
                    // hasDriver Employee
                    com_ind.addProperty(cd_pro, employee.get(company_index));
                } else if (k >= 10 && k <= 17) {
                    // hasOfficeAssistant Employee
                    com_ind.addProperty(coa_pro, employee.get(company_index));
                } else if (k >= 18 && k <= EMPLOYEE_NUM / 4) {
                    // hasStorageAssistant Employee
                    com_ind.addProperty(csa_pro, employee.get(company_index));
                }
                company_index++;
            }

            for (int k = 1; k <= 2; k++) {
                // isOwnerOf Vehicle
                com_ind.addProperty(cow_pro, vehicle.get(vehicle_index));
                vehicle_index++;
                
            }

            delivery_index++;

            company.add(com_ind);
        }
    }


    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String filename = setOntologyUri("courier.owl");
        OntModel model =
                ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM_RULE_INF, null);
        model.read(filename);
        
        populateModel(model);

        String queryB1 = "PREFIX courier:<http://www.courier.com/ontologies/courier.owl#> "
                + "SELECT DISTINCT ?driver ?name ?company "
                + "WHERE { "
                + "	?delivery courier:hasDeliveryVehicle ?vehicle ; "
                + "		courier:hasDeliveryDriver ?driver . "
                + "	?driver courier:hasPersonName ?name . "
                + "     ?vehicle courier:belongsTo courier:Company_1"
                + "}";

        String queryB2 = "PREFIX courier:<http://www.courier.com/ontologies/courier.owl#> "
                + "SELECT DISTINCT ?name "
                + "WHERE { "
                + "	?driver courier:hasPersonName ?name . "
                + "	?company courier:hasDriver ?driver . "
                + "} "
                + "LIMIT 20";
                
        String queryB3 = "PREFIX courier:<http://www.courier.com/ontologies/courier.owl#> "
                + "SELECT DISTINCT ?sender ?name "
                + "WHERE { "
                + "	?delivery courier:hasDeliveryContent ?deliverable ; "
                + "		courier:hasDeliveryVehicle ?vehicle . "
                + "	courier:Company_1 courier:isOwnerOf ?vehicle . "
                + "	?deliverable courier:hasDeliverableSource ?sender . "
                + "	?sender courier:hasPersonName ?name "
                + "}";

        executeQuery(queryB1, model);
        executeQuery(queryB2, model);
        executeQuery(queryB3, model);
    }

    public static void executeQuery(String queryString, OntModel model) {
        Query query = QueryFactory.create(queryString);
        QueryExecution qe = QueryExecutionFactory.create(query, model);
        ResultSet results = qe.execSelect();
        ResultSetFormatter.out(System.out, results, query);
        qe.close();
    }
}

